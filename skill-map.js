
var MwoSkillMapping_A = {
    // Firepower: Weapons
      7: ["Weapons", "Range", 1],
      6: ["Weapons", "Range", 2],
      5: ["Weapons", "Range", 3],
      3: ["Weapons", "Range", 4],
      1: ["Weapons", "Range", 5],
     15: ["Weapons", "Range", 6],
      4: ["Weapons", "Cooldown", 1],
      2: ["Weapons", "Velocity", 1],
      0: ["Weapons", "Velocity", 2],
     14: ["Weapons", "Cooldown", 2],
     12: ["Weapons", "Cooldown", 3],
     10: ["Weapons", "High Explosive", 1],
      9: ["Weapons", "Cooldown", 4],
      8: ["Weapons", "Gauss Charge", 1],
     22: ["Weapons", "Cooldown", 5],
     13: ["Weapons", "Range", 7],
     11: ["Weapons", "Heat Gen", 1],
    234: ["Weapons", "Flamer Ventilation", 1],
     23: ["Weapons", "Heat Gen", 2],
     21: ["Weapons", "Range", 8],
     20: ["Weapons", "Range", 9],
     18: ["Weapons", "Missile Spread", 1],
     16: ["Weapons", "Heat Gen", 3],
     30: ["Weapons", "Cooldown", 6],
     28: ["Weapons", "Heat Gen", 4],
     26: ["Weapons", "LBX Spread", 1],
     24: ["Weapons", "Range", 10],
     19: ["Weapons", "Cooldown", 7],
     17: ["Weapons", "Velocity", 3],
     31: ["Weapons", "Range", 11],
     29: ["Weapons", "Range", 12],
     27: ["Weapons", "Velocity", 4],
     25: ["Weapons", "Cooldown", 8],
     39: ["Weapons", "Enhanced UAC/RAC", 1],
     37: ["Weapons", "Heat Gen", 5],
     35: ["Weapons", "Heat Gen", 6],
     33: ["Weapons", "Range", 13],
     47: ["Weapons", "Laser Duration", 1],
     45: ["Weapons", "Range", 14],
     43: ["Weapons", "Cooldown", 9],
     41: ["Weapons", "Heat Gen", 7],
     38: ["Weapons", "Cooldown", 10],
     36: ["Weapons", "High Explosive", 2],
     34: ["Weapons", "Heat Gen", 8],
     32: ["Weapons", "Missile Rack", 1],
     46: ["Weapons", "Magazine Capacity", 1],
     44: ["Weapons", "Heat Gen", 9],
     42: ["Weapons", "Gauss Charge", 2],
     40: ["Weapons", "Cooldown", 11],
     55: ["Weapons", "Missile Rack", 2],
     54: ["Weapons", "Laser Duration", 2],
    233: ["Weapons", "Flamer Ventilation", 2],
     51: ["Weapons", "Cooldown", 12],
     48: ["Weapons", "Laser Duration", 3],
     63: ["Weapons", "Magazine Capacity", 2],
     53: ["Weapons", "Cooldown", 13],
     52: ["Weapons", "Range", 15],
     50: ["Weapons", "Velocity", 5],
     49: ["Weapons", "Cooldown", 14],
     62: ["Weapons", "Heat Gen", 10],
     61: ["Weapons", "Heat Gen", 11],
     59: ["Weapons", "Cooldown", 15],
     57: ["Weapons", "Heat Gen", 12],
     71: ["Weapons", "Cooldown", 16],
     60: ["Weapons", "Missile Spread", 2],
     58: ["Weapons", "Enhanced UAC/RAC", 2],
     56: ["Weapons", "LBX Spread", 2],
     70: ["Weapons", "Heat Gen", 13],
     69: ["Weapons", "Laser Duration", 4],
     68: ["Weapons", "Heat Gen", 14],

    // Survival: Armor Structure
     67: ["Armor Structure", "Reinforced Casing", 1],
     66: ["Armor Structure", "Shock Absorbance", 1],
     65: ["Armor Structure", "AMS Overload", 1],
     79: ["Armor Structure", "Skeletal Density", 1],
     64: ["Armor Structure", "Reinforced Casing", 2],
     78: ["Armor Structure", "Reinforced Casing", 3],
     76: ["Armor Structure", "Armor Hardening", 1],
     73: ["Armor Structure", "Skeletal Density", 2],
     77: ["Armor Structure", "Shock Absorbance", 2],
     75: ["Armor Structure", "AMS Overload", 2],
     74: ["Armor Structure", "Shock Absorbance", 3],
     72: ["Armor Structure", "Shock Absorbance", 4],
     87: ["Armor Structure", "Armor Hardening", 2],
     85: ["Armor Structure", "Skeletal Density", 3],
     84: ["Armor Structure", "Skeletal Density", 4],
     83: ["Armor Structure", "Reinforced Casing", 4],
     81: ["Armor Structure", "Armor Hardening", 3],
     86: ["Armor Structure", "Reinforced Casing", 5],
     82: ["Armor Structure", "Reinforced Casing", 7],
     80: ["Armor Structure", "Armor Hardening", 4],
     95: ["Armor Structure", "Armor Hardening", 5],
     93: ["Armor Structure", "Skeletal Density", 5],
     91: ["Armor Structure", "Skeletal Density", 6],
     90: ["Armor Structure", "Skeletal Density", 7],
     94: ["Armor Structure", "Reinforced Casing", 6],
     92: ["Armor Structure", "Reinforced Casing", 8],
     89: ["Armor Structure", "Armor Hardening", 6],
    102: ["Armor Structure", "Skeletal Density", 8],
     88: ["Armor Structure", "Skeletal Density", 9],
    103: ["Armor Structure", "Skeletal Density", 10],
    101: ["Armor Structure", "Armor Hardening", 7],
     99: ["Armor Structure", "Shock Absorbance", 5],
     97: ["Armor Structure", "Armor Hardening", 8],
    100: ["Armor Structure", "Armor Hardening", 9],
     98: ["Armor Structure", "Armor Hardening", 10],

    // Mobility: Agility
     96: ["Agility", "Kinetic Burst", 1],
    111: ["Agility", "Torso Yaw", 1],
    110: ["Agility", "Hard Brake", 1],
    108: ["Agility", "Hard Brake", 2],
    106: ["Agility", "Kinetic Burst", 2],
    104: ["Agility", "Torso Yaw", 2],
    109: ["Agility", "Torso Speed", 1],
    107: ["Agility", "Torso Yaw", 3],
    105: ["Agility", "Hard Brake", 3],
    119: ["Agility", "Anchor Turn", 1],
    117: ["Agility", "Kinetic Burst", 3],
    114: ["Agility", "Kinetic Burst", 4],
    118: ["Agility", "Hard Brake", 4],
    116: ["Agility", "Torso Yaw", 4],
    115: ["Agility", "Kinetic Burst", 5],
    113: ["Agility", "Kinetic Burst", 6],
    112: ["Agility", "Torso Speed", 2],
    126: ["Agility", "Torso Speed", 3],
    124: ["Agility", "Torso Pitch", 1],
    122: ["Agility", "Kinetic Burst", 7],
    120: ["Agility", "Hard Brake", 5],
    127: ["Agility", "Torso Yaw", 5],
    125: ["Agility", "Torso Pitch", 2],
    123: ["Agility", "Torso Pitch", 3],
    121: ["Agility", "Anchor Turn", 2],
    134: ["Agility", "Hard Brake", 6],
    131: ["Agility", "Hard Brake", 7],
    135: ["Agility", "Anchor Turn", 3],
    133: ["Agility", "Torso Speed", 4],
    132: ["Agility", "Torso Speed", 5],
    130: ["Agility", "Anchor Turn", 4],
    128: ["Agility", "Torso Pitch", 4],
    143: ["Agility", "Torso Pitch", 5],
    129: ["Agility", "Speed Tweak", 1],
    142: ["Agility", "Speed Tweak", 2],
    141: ["Agility", "Torso Speed", 6],
    138: ["Agility", "Anchor Turn", 5],
    140: ["Agility", "Speed Tweak", 3],
    139: ["Agility", "Speed Tweak", 4],
    137: ["Agility", "Speed Tweak", 5],

    // Jump Jets: Jump Capabilities
    136: ["Jump Capabilities", "Heat Shielding", 1],
    151: ["Jump Capabilities", "Vent Calibration", 1],
    150: ["Jump Capabilities", "Vectoring", 1],
    148: ["Jump Capabilities", "Heat Shielding", 2],
    146: ["Jump Capabilities", "Lift Speed", 1],
    149: ["Jump Capabilities", "Vent Calibration", 2],
    147: ["Jump Capabilities", "Vectoring", 2],
    144: ["Jump Capabilities", "Heat Shielding", 3],
    158: ["Jump Capabilities", "Lift Speed", 2],
    145: ["Jump Capabilities", "Vent Calibration", 3],
    159: ["Jump Capabilities", "Vectoring", 3],
    157: ["Jump Capabilities", "Lift Speed", 3],
    155: ["Jump Capabilities", "Heat Shielding", 4],
    156: ["Jump Capabilities", "Vent Calibration", 4],
    154: ["Jump Capabilities", "Vectoring", 4],
    153: ["Jump Capabilities", "Lift Speed", 4],
    167: ["Jump Capabilities", "Heat Shielding", 5],
    152: ["Jump Capabilities", "Vent Calibration", 5],
    166: ["Jump Capabilities", "Vectoring", 5],
    165: ["Jump Capabilities", "Lift Speed", 5],

    // Operations: Mech Operations
    164: ["Mech Operations", "Quick Ignition", 1],
    163: ["Mech Operations", "Speed Retention", 1],
    162: ["Mech Operations", "Improved Gyros", 1],
    160: ["Mech Operations", "Heat Containment", 1],
    161: ["Mech Operations", "Hill Climb", 1],
    175: ["Mech Operations", "Improved Gyros", 2],
    173: ["Mech Operations", "Heat Containment", 2],
    170: ["Mech Operations", "Heat Containment", 3],
    174: ["Mech Operations", "Cool Run", 1],
    172: ["Mech Operations", "Hill Climb", 2],
    171: ["Mech Operations", "Speed Retention", 2],
    169: ["Mech Operations", "Cool Run", 2],
    168: ["Mech Operations", "Quick Ignition", 2],
    182: ["Mech Operations", "Cool Run", 3],
    180: ["Mech Operations", "Quick Ignition", 3],
    183: ["Mech Operations", "Hill Climb", 3],
    181: ["Mech Operations", "Improved Gyros", 3],
    179: ["Mech Operations", "Heat Containment", 4],
    177: ["Mech Operations", "Quick Ignition", 4],
    191: ["Mech Operations", "Heat Containment", 5],
    178: ["Mech Operations", "Improved Gyros", 4],
    176: ["Mech Operations", "Speed Retention", 3],
    189: ["Mech Operations", "Quick Ignition", 5],
    190: ["Mech Operations", "Cool Run", 4],
    188: ["Mech Operations", "Cool Run", 5],

    // Sensors: Sensor Systems
    187: ["Sensor Systems", "Target Info Gathering", 1],
    186: ["Sensor Systems", "Target Decay", 1],
    185: ["Sensor Systems", "Sensor Range", 1],
    184: ["Sensor Systems", "Target Retention", 1],
    198: ["Sensor Systems", "Sensor Range", 2],
    196: ["Sensor Systems", "Target Info Gathering", 2],
    199: ["Sensor Systems", "Target Info Gathering", 3],
    197: ["Sensor Systems", "Sensor Range", 3],
    195: ["Sensor Systems", "Advanced Zoom", 0],
    193: ["Sensor Systems", "Target Decay", 2],
    194: ["Sensor Systems", "Target Info Gathering", 4],
    192: ["Sensor Systems", "Target Retention", 2],
    207: ["Sensor Systems", "Radar Deprivation", 1],
    204: ["Sensor Systems", "Target Decay", 3],
    206: ["Sensor Systems", "Sensor Range", 4],
    205: ["Sensor Systems", "Sensor Range", 5],
    203: ["Sensor Systems", "Seismic Sensor", 1],
    201: ["Sensor Systems", "Radar Deprivation", 2],
    215: ["Sensor Systems", "Target Decay", 4],
    213: ["Sensor Systems", "Radar Deprivation", 3],
    202: ["Sensor Systems", "Enhanced ECM", 1],
    200: ["Sensor Systems", "Target Info Gathering", 5],
    214: ["Sensor Systems", "Target Decay", 5],
    212: ["Sensor Systems", "Enhanced ECM", 2],
    211: ["Sensor Systems", "Seismic Sensor", 2],
    210: ["Sensor Systems", "Radar Deprivation", 4],
    209: ["Sensor Systems", "Radar Deprivation", 5],

    // Auxillary: Miscellaneous
    208: ["Miscellaneous", "Consumable Slot", 1],
    223: ["Miscellaneous", "UAV Duration", 0],
    222: ["Miscellaneous", "Advanced Salvos", 1],
    220: ["Miscellaneous", "UAV Range", 1],
    218: ["Miscellaneous", "Enhanced Coolshot", 1],
    216: ["Miscellaneous", "Advanced Salvos", 2],
    221: ["Miscellaneous", "Extra UAV", 0],
    219: ["Miscellaneous", "Capture Assist", 1],
    217: ["Miscellaneous", "Capture Assist", 2],
    231: ["Miscellaneous", "Expanded Reserves", 0],
    230: ["Miscellaneous", "UAV Range", 2],
    228: ["Miscellaneous", "Enhanced NARC", 1],
    226: ["Miscellaneous", "Enhanced Coolshot", 2],
    224: ["Miscellaneous", "Enhanced NARC", 2],
    238: ["Miscellaneous", "Enhanced Spotting", 0],
    229: ["Miscellaneous", "Consumable Slot", 2],
    227: ["Miscellaneous", "Capture Assist", 3],
    225: ["Miscellaneous", "Capture Assist", 4],
    239: ["Miscellaneous", "Consumable Slot", 3],
    236: ["Miscellaneous", "Coolant Reserves", 0],
    237: ["Miscellaneous", "Coolshot Cooldown", 0],
    235: ["Miscellaneous", "Consumable Slot", 4],
};

function MwoSkillImporter() {
    this.latest = 'a';
    this.maxlength = 60;
};

MwoSkillImporter.prototype.serialize = function(nodes) {
    var indexes = [];
    for (var n of nodes) {
        for (var k of Object.keys(MwoSkillMapping_A)) {
            var v = MwoSkillMapping_A[k];
            if (n[0] == v[0] && n[1] == v[1] && n[2] == v[2]) {
                indexes.push(parseInt(k));
                break;
            }
        }
    }

    indexes.sort(function (a, b) { return a - b; });

    var code = 'a';
    for (var offset = 0; offset < 60; offset += 1) {
        var v = 0;
        while (indexes[0] < (offset + 1) * 4) {
            var bit = (offset + 1) * 4 - indexes.shift();
            switch (bit) {
                case 4:
                    v |= 8;
                    break;
                case 3:
                    v |= 4;
                    break;
                case 2:
                    v |= 2;
                    break;
                case 1:
                    v |= 1;
                    break;
            }
        }
        code += v.toString(16);
    }

    return code;
};

MwoSkillImporter.prototype.deserialize = function(code) {
    if (!code) {
        return [];
    }

    // TODO: strip newline, whitespace

    if (code[0] == 'a' && code.length == 61) {
        var mapping = MwoSkillMapping_A;

        var offset = 1;
        var selection = [];
        while (offset < code.length) {
            var value = parseInt(code[offset], 16);
            if (value & 1) {
                // TODO: make sure mapping is valid
                selection.push(mapping[(offset - 1) * 4 + 3]);
            }
            if (value & 2) {
                selection.push(mapping[(offset - 1) * 4 + 2]);
            }
            if (value & 4) {
                selection.push(mapping[(offset - 1) * 4 + 1]);
            }
            if (value & 8) {
                selection.push(mapping[(offset - 1) * 4 + 0]);
            }

            offset += 1;
        }

        return selection;
    }

    return [];
};
